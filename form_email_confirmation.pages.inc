<?php
/**
 * @file form_email_confirmation.pages.inc
 *
 * Contains page callbacks for the Form email confirmation module.
 */

/**
 * Page callback to inform the user that an email has been sent to them.
 */
function form_email_confirmation_sent() {
  $output = t("Thank you for your submission. A request for confirmation has been sent to your e-mail address.");
  
  return $output;
}

/**
 * Page callback for user to confirm their email.
 *
 * This is similar to user_pass_reset().
 */ 
function form_email_confirmation_confirm(&$form_state, $sid, $timestamp, $hash, $action = NULL) {
  $form = array();
  // Some checks.
  // We're following the example of user_pass_reset() but reworking the logic 
  // into less of a spaghetti mess!
  
  // Time out, in seconds, until login URL expires. 24 hours = 86400 seconds.
  $timeout = 86400;
  $current = time();

  if ($timestamp > $current) {
    drupal_access_denied();
    return;
  }

  // No time out for first time login.
  if ($current - $timestamp > $timeout) {
    drupal_set_message(t('You have tried to use an email confirmation link that has expired.'));
    drupal_goto(variable_get('site_frontpage', 'node'));
    return;
    // drupal_goto() exits; return is just for the look of it.
  }
  
  // Retrieve our submission record from the database.
  //$result = db_query("SELECT * FROM {form_email_confirmation} WHERE sid = %d", $sid);
  $form_submission_record = db_fetch_array(db_query("SELECT * FROM {form_email_confirmation} WHERE sid = %d", $sid));
  //dpm($form_submission_record, 'record');
  
  $stored_form_id = $form_submission_record['form_id'];
  $stored_form_state = unserialize($form_submission_record['form_state']);
  $mail = $form_submission_record['mail'];

  // Verify the hashed token in the URL; if invalid send the user to the home page.
  if ($hash != form_email_confirmation_rehash($stored_form_id, $timestamp, $mail)) {
    //dpr('FAIL');
    watchdog('form_email_confirmation', "HASH FAIL $sid: $stored_form_id, $timestamp, $mail");
    drupal_set_message(t('You have tried to use an email confirmation link which has either been used or is no longer valid.'));
    drupal_goto(variable_get('site_frontpage', 'node'));
    return;
  }
  
  // First stage is a confirmation form, then confirm action.
  if ($action == 'confirm') {
    // Retrieve the original options from the stored form state values.
    $options = $stored_form_state['values']['form_email_confirmation_options'];
    
    watchdog('form_email_confirmation', 'Anonymous user at !email used one-time confirmation link at time %timestamp.', array(
      '%email' => $mail,
      '%timestamp' => $current,
    ));
    drupal_set_message(t('You have just used a one-time email confirmation link. Your form submmission has been processed.'));

    // Execute the original form, passing a parameter of TRUE to the original
    // form builder's $email_confirmed parameter.
    // This means that we go through the form's normal submit process this time
    // around.
    drupal_execute($stored_form_id, $stored_form_state, TRUE);

    // Delete our record.
    db_query("DELETE FROM {form_email_confirmation} WHERE sid = %d", $sid);
    
    // Send the user to the path given in the options. This defaults to the
    // path the original form was on.
    drupal_goto($options['path_submit']); 
    return $form;   
  }
  else {    
    $form['message'] = array('#value' => t('<p>This is a one-time email confirmation for %mail and will expire on %expiration_date.</p><p>Click on this button to confirm your form submission.</p>', array(
      '%mail' => $mail, 
      '%expiration_date' => format_date($timestamp + $timeout),
    )));
    $form['help'] = array('#value' => '<p>'. t('This link can be used only once.') .'</p>');
    $form['submit'] = array('#type' => 'submit', '#value' => t('Confirm submission'));
    $form['#action'] = url("email-confirmation/confirm/$sid/$timestamp/$hash/confirm");
    return $form;
  }
}
